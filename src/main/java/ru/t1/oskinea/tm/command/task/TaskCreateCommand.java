package ru.t1.oskinea.tm.command.task;

import ru.t1.oskinea.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Create new task.";

    private static final String NAME = "task-create";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
