package ru.t1.oskinea.tm.command.user;

import ru.t1.oskinea.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Change password for current user.";

    private final String NAME = "user-change-password";

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.print("ENTER NEW PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
