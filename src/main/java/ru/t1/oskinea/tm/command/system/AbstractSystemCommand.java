package ru.t1.oskinea.tm.command.system;

import ru.t1.oskinea.tm.api.service.ICommandService;
import ru.t1.oskinea.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
