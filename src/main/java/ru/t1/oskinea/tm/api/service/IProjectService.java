package ru.t1.oskinea.tm.api.service;

import ru.t1.oskinea.tm.api.repository.IProjectRepository;
import ru.t1.oskinea.tm.enumerated.Sort;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.model.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project create(String name);

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
