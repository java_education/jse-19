package ru.t1.oskinea.tm.api.service;

import ru.t1.oskinea.tm.model.User;

public interface IAuthService {

    User getUser();

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

}
